﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int ScoreRight = 0;
    public int ScoreLeft = 0;

    public Transform SpawnPoint;
    public Rigidbody2D BallRB;

    public Transform Car1;
    public Transform Car1SpawnPoint;
    public Rigidbody2D Car1RB;

    void Score(int ScoreLeft, int ScoreRight) {}

    void Goal()
    {
        //ball position set up
        BallRB.velocity = new Vector2(0, 0);
        transform.position = SpawnPoint.transform.position;
        transform.rotation = SpawnPoint.transform.rotation;
        BallRB.freezeRotation = true;
        BallRB.freezeRotation = false;

        //car1 position set up
        Vector3 theScale = Car1.transform.localScale;

        Car1.transform.position = Car1SpawnPoint.transform.position;
        Car1.transform.rotation = Car1SpawnPoint.transform.rotation;
        Car1RB.velocity = new Vector2(0, 0);
        theScale.x = 1;
        theScale.y = 1;
        Car1.transform.localScale = theScale;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("GoalRight"))
        {
            ScoreRight++;
            Goal();
            Debug.Log("Goal!");
            Score(ScoreLeft, ScoreRight);
        }
        if (collision.gameObject.CompareTag("GoalLeft"))
        {
            ScoreLeft++;
            Goal();
            Debug.Log("Goal!");
            Score(ScoreLeft, ScoreRight);
        }
    }
}
