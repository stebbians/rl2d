﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketEngine : MonoBehaviour
{
    public CarMovement controller;

    public ParticleSystem ThrusterPath;

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            ThrusterPath.Emit(10);
        }
    }
}
