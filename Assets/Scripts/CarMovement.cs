﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    //variables
    public float JumpForce = 400f;
    public float CarSpeed = 40f;
    public float RotationSpeed = 5f;
    public float ThrusterPower = 40f;
    public float MovementSmoothing = .05f;
    public static Vector2 Position;

    //stuff to check
    public LayerMask WhatIsGround;
    public Transform GroundCheck;
    public Transform OnTheRoofCheck;
    public Transform LookDirection;  

    //ground and onRoof define
    private bool Grounded;
    const float GroundedRadius = .2f;
    const float OnTheRoofRadius = .2f;

    //triggers
    private bool FacingRight = true;
    bool jump = false;
    bool inAir = false;
    bool OnTheRoof = false;
    bool candoublejump = false;
    bool WasOnTheRoof = false;
    bool OnTheRightWall = false;
    bool OnTheLeftWall = false;

    private Vector3 Velocity = Vector3.zero;
    public Rigidbody2D rb;
    float horizontalMove = 0f;

    public void RocketEngine() //simulates independent rocket engine
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //need optimization
            if (!WasOnTheRoof)
            {
                if (transform.localScale.x == 1)
                {
                    rb.AddForce(transform.right * ThrusterPower);
                }
                else
                {
                    rb.AddForce(transform.right * -ThrusterPower);
                }
            }
            else
            {
                if (transform.localScale.x == 1)
                {
                    rb.AddForce(transform.right * ThrusterPower);
                }
                else
                {
                    rb.AddForce(transform.right * -ThrusterPower);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        RocketEngine();

        Grounded = false;
        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, GroundedRadius, WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                Grounded = true;
            }
        }

        //inputs for MOVE method
        Move(horizontalMove * Time.fixedDeltaTime, jump);
        jump = false;
    }

    //Movement callculation method
    public void Move(float move, bool jump)
    {
        if (Grounded)
        {
            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(move * 10f, rb.velocity.y);
            // And then smoothing it out and applying it to the character
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref Velocity, MovementSmoothing); 
        }
        //Flip Player
        if (!OnTheRoof) 
        {
            if (Input.GetKeyDown(KeyCode.LeftControl) && !FacingRight)
            {
                Flip();
            }
            else if (Input.GetKeyDown(KeyCode.LeftControl) && FacingRight)
            {
                Flip();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Movement manager 
        //Lowering speed if going backwards
        if (!WasOnTheRoof)
        {
            if (FacingRight && Input.GetAxisRaw("Horizontal") == -1)
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * (CarSpeed / 3) * 2;
            }
            else if (!FacingRight && Input.GetAxisRaw("Horizontal") == 1)
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * (CarSpeed / 3) * 2;
            }
            else
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * CarSpeed;
            }
        }
        else
        {
            if (FacingRight && Input.GetAxisRaw("Horizontal") == 1)
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * (CarSpeed / 3) * 2;
            }
            else if (!FacingRight && Input.GetAxisRaw("Horizontal") == -1)
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * (CarSpeed / 3) * 2;
            }
            else
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * CarSpeed;
            }
        }

        //Jump manager
        //Double jump, wall jump and recovery jump
        if (Input.GetButtonDown("Jump"))
        {
            //recovery jump
            if (OnTheRoof && !Grounded)
            {
                //disable double jump
                candoublejump = false;
                //make a little jump up
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(new Vector2(0, (JumpForce / 4) * 3));
                //flip from the roof by inverting scale for Y axis
                Vector3 theScale = transform.localScale;
                theScale.y *= -1;
                transform.localScale = theScale;
                //not on the roof anymore
                OnTheRoof = false;
                WasOnTheRoof = !WasOnTheRoof;
            }
            if (Grounded)
            {
                OnTheRoof = false;
                rb.velocity = new Vector2(rb.velocity.x, 0); //nulify any force on Y
                rb.AddForce(new Vector2(0, JumpForce)); //add Y force
                candoublejump = true; //enable second jump
            }
            else
            {
                // double jump
                if (candoublejump)
                {
                    candoublejump = false;
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    rb.AddForce(new Vector2(0, JumpForce));
                }
            }
            //Off-Wall Jumps
            if (OnTheLeftWall)
            {
                    rb.velocity = new Vector2(rb.velocity.x, 0); //nulify any force on Y
                    rb.AddForce(new Vector2(JumpForce, 0)); //add Y force
                    candoublejump = true; //enable second jump
                    OnTheLeftWall = false;
            }         
            if (OnTheRightWall)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0); //nulify any force on Y
                rb.AddForce(new Vector2(-JumpForce, 0)); //add Y force
                candoublejump = true; //enable second jump
                OnTheRightWall = false;
            }
            else if (!OnTheLeftWall && !OnTheRightWall && !Grounded)
            {
                // double jump
                if (candoublejump)
                {
                    candoublejump = false;
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    rb.AddForce(new Vector2(0, JumpForce));
                }
            }
        }

        //Spin if not grounded (in air)
        if (!Grounded)
        {
            rb.AddTorque(RotationSpeed * 100f * Time.fixedDeltaTime * -Input.GetAxisRaw("Horizontal"), ForceMode2D.Force);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) //compare tags to trigger intended behavoir 
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            inAir = false; //not in use
            //Check if car lying on the roof
            Collider2D[] roofColliders = Physics2D.OverlapCircleAll(OnTheRoofCheck.position, OnTheRoofRadius, WhatIsGround);
            for (int i = 0; i < roofColliders.Length; i++)
            {
                if (roofColliders[i].gameObject != gameObject)
                {
                    OnTheRoof = true;
                }
            }
        }
        if (collision.gameObject.CompareTag("OnTheRightWall")) //check if on the right wall
        {
            OnTheRightWall = true;
        }
        if (collision.gameObject.CompareTag("OnTheLeftWall")) //check if on the left wall
        {
            OnTheLeftWall = true;
        }
    }

    private void Flip() //for flipping player
    {
            // Switch the way the player is labelled as facing.
            FacingRight = !FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
    }
}