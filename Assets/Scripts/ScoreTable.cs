﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreTable : MonoBehaviour
{
    public Collider2D Ball;
    public TextMeshProUGUI score;

    public BoxCollider2D blue;
    public BoxCollider2D red;

    private int scoreRed = 0;
    private int scoreBlue = 0;

    void FixedUpdate()
    {
        if (Ball.IsTouching(blue))
        {
            score.text = ++scoreRed + " - " + scoreBlue;
            return;
        }
        if (Ball.IsTouching(red))
        {
            score.text = scoreRed + " - " + ++scoreBlue;
            return;
        }
    }
}
